//Contains everything that GOD may want to use....
#include <algorithm>
#include "treeGame.hpp"
#include "tree.hpp"


namespace {
    // Should not collide with other files
    int godDidSameTurns = 0;

    std::vector<std::pair<World *, int>> worldsWithDistances;


    struct myECmpFT {
        bool operator()(World *i, World *j) { return (i->getEvilLevel() > j->getEvilLevel()); }
    } myECmpF;

    bool calDistF(World *curr, int id) {
        std::pair<World *, int> p;
        p.first = curr;
        p.second = worldsWithDistances[curr->getParent()->getId()].second + 1;
        worldsWithDistances.emplace(worldsWithDistances.begin() + id, p);
        return true;
    }

    bool myDCmpF(int d1, int d2) {
        return d1 < d2;
    };

}

void Tree::startTheGame(godF godFunc, FILE *inFile) {
    World *currPos =  findByName(GREAT_PIT_NAME);
    World *prevPos; //TO make them different
    while (godDidSameTurns < GOD_MAX_SAME_TURNS) {
        prevPos = currPos;
        currPos = godFunc(this, currPos);
        currPos->_heal();
        update();
        printTreeToFile(inFile, currPos);
        if (prevPos == currPos) godDidSameTurns++;
        else godDidSameTurns = 0;
    }
}

std::vector<World *> Tree::getWorldsSortedByEvilDesc() {
//    std::vector<World *> vec(_worlds, _worlds + _worldN); //Copies array
//    std::sort(vec.begin(), vec.end(), myECmpF);
    //TODO: Doesn't work:
    /*std::sort(vec.begin(), vec.end(),
              [] (World* lhs, const auto& rhs) {
                  return lhs->getEvilLevel() < rhs->getEvilLevel();
              });
          */
//    return vec;
}


std::vector<std::pair<World *, int>> Tree::getWorldsWithDist(World *start) {
    BFS_resetStatus();
    std::pair<World *, int> p;
    p.first = start;
    p.second = 0;
    worldsWithDistances.emplace(worldsWithDistances.begin() + start->index, p);
    BFS(start, calDistF);
    return worldsWithDistances;
}

std::vector<std::pair<World *, int>> Tree::getWorldsWithEvilLevel() { //FIXME
    std::vector<std::pair<World *, int>> worldsWithEvilLevel;
    std::pair<World *, int> p;
    for (auto j:_worlds ) {
        p.first = j.second;
        p.second = j.second->getEvilLevel();
        worldsWithEvilLevel.push_back(p);
    }
    return worldsWithEvilLevel;
}
