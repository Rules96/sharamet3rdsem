#include <queue>
#include <algorithm>
#include "god.hpp"
//Includes treeIO and treeGame
#include "tree.hpp"
//#include "treeGame.hpp"
//#include "cstdio"
//#include <vector>

namespace {
    std::vector<World *> worlds;

    bool fetchWorldF(World *curr, int id) {
        worlds.push_back(curr);
        return true;
    }

    int choice;

    bool cmpf(std::pair<World *, int> el1, std::pair<World *, int> el2) {
        return el1.second < el2.second;
    }
}

//TODO this must correspond with godF !!!
//How do I declare it?
//FIXME REWRITE EVERY SAME FUNCTION (using global variables) USING FUNCTORS
World *godMain_Asking(Tree *tree, World *current) {
    worlds.clear();
    printf("At %s.Enter one of the following world number to go:\n", current->getName());
    tree->BFS_resetStatus();
    tree->BFS(tree->getRoot(), fetchWorldF);
    for (unsigned int i = 0; i < worlds.size(); i++) {
        printf("%d: %s, ", i, worlds[i]->getName());
    }
    printf("\n->: ");
    scanf("%d", &choice);
    return worlds[choice];
}

World *godMain(Tree *tree, World *current) {
    worlds.clear();
//    std::vector<World*> worldsByEvilDesc = tree->getWorldsSortedByEvilDesc();
    std::vector<std::pair<World *, int>> worldsWithDistance = tree->getWorldsWithDist(current);
    std::vector<std::pair<World *, int>> worldsWithEvil = tree->getWorldsWithEvilLevel();
    std::vector<std::pair<World *, int>> worldsWithCharacteristic;
    std::pair<World *, int> p;
    int maxEvilLEvel = 1;
    for (int i = 0; i < worldsWithEvil.size(); i++) {
        if (worldsWithEvil[i].second > maxEvilLEvel) {
            maxEvilLEvel = worldsWithEvil[i].second;
        }
    }

    for (int i = 0; i < worldsWithDistance.size(); i++) {
        p.first = worldsWithDistance[i].first;
        p.second = worldsWithDistance[i].second * maxEvilLEvel;
        p.second += worldsWithEvil[i].second;
        //auto it=std::max_element(worldsWithEvil);
        worldsWithCharacteristic.push_back(p);
    }
    std::sort(worldsWithCharacteristic.begin(), worldsWithCharacteristic.end(), cmpf);
    return worldsWithCharacteristic[0].first;
}

