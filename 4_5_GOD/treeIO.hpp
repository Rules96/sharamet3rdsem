#ifndef INC_4_TREEIO_H
#define INC_4_TREEIO_H

#include <cstdio>

#define MAX_STR_INPUT_LEN 255 //


enum RTFF_ERROR {
    RTFF_SUCCESS, RTFF_FILE_NULL, RTFF_WORLD_WRONG_PARENT, RTFF_ASGARD_MISSING, RTFF_GREAT_PIT_MISSING
};

//-PRINT
struct trunk {
    struct trunk *prev;
    const char *str;
};

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

enum PTTF_ERROR {
    PTTF_SUCCESS, PTTF_FILE_NULL
};

#endif //INC_4_TREEIO_H
