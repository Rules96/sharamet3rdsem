#ifndef INC_4_GOD_HPP
#define INC_4_GOD_HPP

class Tree;

class World;

World *godMain_Asking(Tree *tree, World *current);

World *godMain(Tree *tree, World *current);

#endif //INC_4_GOD_HPP
