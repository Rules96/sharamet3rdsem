//Includes treeIO and treeGame
#include "tree.hpp"
#include "god.hpp"

void treeControl(Tree *tree) {
    //This function is a friend of tree
    //it can be used to call private methods
}

void worldControl(World *world) {
    //This function is a friend of world
    //it can be used to call private methods
}

int main() {
    int e;
    Tree *tree = new Tree();
    FILE *infile = fopen("input.txt", "r");
    if ((e = tree->readTreeFromFile(infile)) != RTFF_SUCCESS) {
        printf("%d", e);
    } else {
        tree->printTreeToFile(stdout, NULL);
        tree->startTheGame(godMain, stdout);
    }
    return 0;
}