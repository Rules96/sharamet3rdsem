#include <cstring>

#include "treeIO.hpp"
#include "tree.hpp"

void show_trunks(struct trunk *p) {
    if (!p) return;
    show_trunks(p->prev);
    printf("%s", p->str);
}

void print_tree(FILE *infile, World *root, struct trunk *prev, int is_left, World *godIsAt) {
    if (root == NULL) return;

    struct trunk this_disp = {prev, "    "};
    const char *prev_str = this_disp.str;
    print_tree(infile, root->getLeft(), &this_disp, 1, godIsAt);
    if (!prev)
        this_disp.str = "---";
    else if (is_left) {
        this_disp.str = ".--";
        prev_str = "   |";
    } else {
        this_disp.str = "`--";
        prev->str = prev_str;
    }

    show_trunks(&this_disp);
    if (root->getIsDark()) fprintf(infile, ANSI_COLOR_RED);
    else if (root == godIsAt) fprintf(infile, ANSI_COLOR_BLUE);
    fprintf(infile, "%s(%d)\n", root->getName(), root->getEvilLevel());
    if (root->getIsDark() || root == godIsAt) fprintf(infile, ANSI_COLOR_RESET);

    if (prev) prev->str = prev_str;
    this_disp.str = "   |";

    print_tree(infile, root->getRight(), &this_disp, 0, godIsAt);
    if (!prev) fputs("", infile);
}


RTFF_ERROR Tree::readTreeFromFile(FILE *inFile) {
    int n, i;
    char name[2][NAME_MAX_LEN + 1];
    char pos[2][5 + 1]; //for left/right
    char str[MAX_STR_INPUT_LEN];
    World *w, *child;
    if (inFile == NULL) return RTFF_FILE_NULL;
    fscanf(inFile, "%d", &n);
    while (n--) {
        fscanf(inFile, "%s", name[0]);
        fscanf(inFile, "%d", &i);
        if ((w = findByName(name[0])) == NULL) {
            w = new World(name[0]); //There was no tree with such name added before
        }
        w->becomesDarkAt = i;
        fscanf(inFile, "%d", &i);
        w->evilLevel = i;
        w->isDark = w->evilLevel >= w->becomesDarkAt ? 1 : 0;
        fscanf(inFile, "%d", &i);
        w->selfRecoversAt = i;
        add(w);
        fscanf(inFile, "%s", name[0]);
        if ((w->parent = findByName(name[0])) == NULL) {
            return RTFF_WORLD_WRONG_PARENT;
        }
        fgets(str, MAX_STR_INPUT_LEN, inFile);
        i = sscanf(str, "%s %s %s %s", name[0], pos[0], name[1], pos[1]);
        if (i == 2 || i == 4) {
            child = new World(name[0]);
            add(child);
            if (!strcmp(pos[0], "left")) {
                w->left = child;
            } else if (!strcmp(pos[0], "right")) {
                w->right = child;
            }
        }
        if (i == 4) {
            child = new World(name[1]);
            add(child);
            if (!strcmp(pos[1], "left")) {
                w->left = child;
            } else if (!strcmp(pos[1], "right")) {
                w->right = child;
            }
        }
    }
    if (!findByName(ASGARD_NAME)) return RTFF_ASGARD_MISSING;
    if (!findByName(GREAT_PIT_NAME)) return RTFF_GREAT_PIT_MISSING;
    return RTFF_SUCCESS;
}


PTTF_ERROR Tree::printTreeToFile(FILE *outFile, World *godIsAt) {
    if (outFile == NULL) return PTTF_FILE_NULL;
    print_tree(outFile, getRoot(), NULL, 0, godIsAt);
    return PTTF_SUCCESS;
}

