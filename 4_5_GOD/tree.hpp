#ifndef INC_4_TREE_H
#define INC_4_TREE_H

#define ASGARD_NAME "Asgard"
#define GREAT_PIT_NAME  "great_pit"
#define NAME_MAX_LEN 15 //Max world name length
#define UNDEFINED -1 //Each 'int' may be 'undefined'

//---IMPORTS:----
#include <string>
#include <unordered_map>
#include "treeIO.hpp"
#include "treeGame.hpp"
//-------


typedef std::unordered_map<std::string, World*>::iterator WorldIterator;
//returns whether BFS should continue (i.e: false = stop bfs)
typedef bool(*BFS_callback)(World *currentWorld, int uniq_id);

class World {
    friend class Tree; // so that Tree could set index to the world
    friend void worldControl(World *world);

    friend bool worldUpdF(World *curr, int id);

public:
    World(const char name[15]);

    virtual ~World();

    bool update(); //bool whether the world survived the updateBFS

    //Getters:

    int getId() {
        return index;
    }

    const char *getName() const {
        return name;
    }

    int getBecomesDarkAt() const {
        return becomesDarkAt;
    }

    int getEvilLevel() const {
        return evilLevel;
    }

    int getSelfRecoversAt() const {
        return selfRecoversAt;
    }

    int getIsDark() const {
        return isDark;
    }

    World *getParent() const {
        return parent;
    }

    World *getLeft() const {
        return left;
    }

    World *getRight() const {
        return right;
    }

private:
    void _heal();
    char name[NAME_MAX_LEN + 1];
    int becomesDarkAt; //may become DARK if evilLevel >= this
    int evilLevel; // == 0 when healthy
    int selfRecoversAt; //may self recover if evilLevel <= this
    int isDark; // 1/0
    struct World *parent;
    struct World *left;
    struct World *right;
    int index; //index in Tree's _worldNames,_statuses,etc. arrays CLASSES ARE FRIENDS BECAUSE OF THIS
    Tree *tree; //hack to handle self delete

};


class Tree {
    friend void treeControl(Tree *tree);

    friend bool worldUpdF(World *curr, int id); //FIXME causes ANY similar function
    //FIXME i.e bool fetchWorldF(World *curr, int id), be friend
    //FIXME should be restricted to ONLY worldUpdF
public:
    Tree();

    //http://stackoverflow.com/questions/461203/when-to-use-virtual-destructors
    //When tree is destructed the destructors of _worlds in it are NOT called!
    //So you need to manually delete all _worlds until I ask teacher how to manage memory in C++
    virtual ~Tree();

    RTFF_ERROR readTreeFromFile(FILE *inFile); //TODO IS DEPENDENT

    PTTF_ERROR printTreeToFile(FILE *outFile, World *godIsAt); //TODO IS DEPENDENT

    void startTheGame(godF godFunc, FILE *inFile); //TODO IS DEPENDENT
    //TODO SHOULD BE PRIVATE AND SHOULD BE CONTROLLED ONLY BY treeControl()?

    //Find the world in tree by name
    World *findByName(const char *name);

    WorldIterator find(World *w);

    void BFS_resetStatus();

    void BFS(World *s, BFS_callback call_back);

    //Getters:

    std::vector<World *> getWorlds();

    World *getRoot() {
        return _root;
    }

    std::vector<World *> getWorldsSortedByEvilDesc();

    //std::vector<World*> getWorldsSortedByDistFromThisAsc(World *start);
    std::vector<std::pair<World *, int>> getWorldsWithDist(World *start);

    std::vector<std::pair<World *, int>> getWorldsWithEvilLevel();

private:
    std::unordered_map<std::string, World *> _worlds;
    int *_statuses; //For BFS/DFS and other purposes. Should always be of size _worldN!
    bool remove(World *w);

    World *_root;
    /*
 * Returns a pointer to a newly added world
 * !DOESN'T COPY THE WORLD, INSTEAD COPIES POINTER ONLY
 * !IF WORLD WITH THIS NAME WAS ALREADY IN TREE UPDATES
 * WORLD'S PROPERTIES IN TREE AND FREES THE W
 */
    void add(World *w);

    void update();


};


#endif //INC_4_TREE_H
