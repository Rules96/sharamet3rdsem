#ifndef INC_4_TREEGAME_HPP
#define INC_4_TREEGAME_HPP

#include <vector>

class World;

class Tree;

typedef World *(*godF)(Tree *, World *currentPosition);

#define GOD_MAX_SAME_TURNS 5

#endif //INC_4_TREEGAME_HPP
