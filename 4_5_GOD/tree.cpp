#include <cstring>
#include <malloc.h>
#include "tree.hpp"
//For BFS
#include <queue>
#include <iostream>


World::World(const char *_name) {
    strcpy(name, _name);
    becomesDarkAt = UNDEFINED;
    selfRecoversAt = UNDEFINED;
    isDark = UNDEFINED;
    evilLevel = UNDEFINED;
    left = right = parent = NULL;
    index = -1;
}

World::~World() {
    std::cout<< "World::Destructor()"<<std::endl;
}

//bool whether the world survived the update
bool World::update() {
    if (evilLevel >= becomesDarkAt) isDark = 1;
    if ((left == NULL) && (right == NULL)) {
        if (!isDark && evilLevel > 0) {
            if (evilLevel <= selfRecoversAt) _heal();
            else return false;
        } else {
            parent->evilLevel += evilLevel;
        }
    }
    int halfEvilLevel = evilLevel / 2;
    int thirdEvilLevel = evilLevel / 3;
    if ((left != NULL) && (right != NULL)) {
        if (isDark) {
            left->evilLevel += thirdEvilLevel;
            right->evilLevel += thirdEvilLevel;
            parent->evilLevel += thirdEvilLevel;
        } else {
            left->evilLevel += halfEvilLevel;
            right->evilLevel += halfEvilLevel;
            evilLevel = evilLevel - halfEvilLevel;
        }
    } else if ((left != NULL) || (right != NULL)) {
        if (isDark) {
            parent->evilLevel += halfEvilLevel;
            (left != NULL ? left : right)->evilLevel += halfEvilLevel;
        } else {
            (left != NULL ? left : right)->evilLevel += evilLevel;
            evilLevel -= halfEvilLevel;
        }
    }
    if (evilLevel <= selfRecoversAt) _heal();
    return true;
}

void World::_heal() {
    if (isDark) isDark = 0;
    evilLevel = 0;
}

Tree::Tree() {
    _statuses = (int *) calloc(1, sizeof(int));
}


WorldIterator Tree::find(World *w) {
    for (WorldIterator it = _worlds.begin(); it != _worlds.end(); ++it )
        if (it->second == w) return it;
}

World *Tree::findByName(const char *name) {
    WorldIterator el = _worlds.find(name);
    if( el!=_worlds.end())return el->second;
    else return NULL;
}

void Tree::add(World *w) {
    _worlds.insert(std::pair<std::string, World*>(w->name, w));
}

//TODO should  call destructor of the world
bool Tree::remove(World *w) {
    _worlds.erase(find(w));

}

void Tree::BFS_resetStatus() {
    std::fill(_statuses, _statuses + _worlds.size(), 0);//FIXME replace int* with vector<int>
}

void Tree::BFS(World *s, BFS_callback call_back) {
    _statuses[s->index] = 1;
    std::queue<World *> Q;
    Q.push(s);
    while (!Q.empty()) {
        World *u = Q.front();
        Q.pop();
        //for v in Adj(u)
        World *v;
        if (u->left != NULL) {
            v = u->left;
            if (_statuses[v->index] == 0) {
                _statuses[v->index] = 1;
                Q.push(v);
            }
        }
        if (u->right != NULL) {
            v = u->right;
            if (_statuses[v->index] == 0) {
                _statuses[v->index] = 1;
                Q.push(v);
            }
        }
        call_back(u, u->index);
    }
}

//TODO FRIEND
// FIXME Should not collide with other files
//FIXME REWRITE EVERY SAME FUNCTION (using global variables) USING FUNCTORS
bool worldUpdF(World *curr, int id) {
//    curr->tree->printTreeToFile(stdout, curr);
    if (!curr->update()) {
        curr->tree->remove(curr);
        delete curr;//TODO should it be here?
    }
    return true;
}


void Tree::update() {
    BFS_resetStatus();
    BFS(getRoot(), worldUpdF); //TODO FIXME
}

Tree::~Tree() {
    free(this->_statuses);
}

std::vector<World *> Tree::getWorlds() {
    return std::vector<World *>();
}




