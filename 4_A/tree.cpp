//
// Created by Artem Bobrov on 22.01.17.
//
#include "defines.h"
#include "tree.h"

int findWorldsNameInBuffer(int n, vector<world> buffer, string name){
    for(int i = 0; i < n; i++)
        if(buffer[i].name == name)
            return i;
    return -1;
}

void printfOneNodeStdErr(tree node){
    cerr << node.name << endl;
    cerr << "\t" << node.strength << " " << node.evil << " " <<node.cure << endl;
    cerr << "\t" << node.parentName << " ";
    if(node.leftChild)
        cerr << node.leftChildName << " left ";
    if(node.rightChild)
        cerr << node.rightChildName << " right ";
    cerr << endl;
    cerr << "\tOdin, Are you here? ";
    if(node.isOdinHere)
        cerr << "Yes" << endl;
    else
        cerr << "No" << endl;
    cerr << "\tIs Infected? ";
    if(node.isInfectedWorld)
        cerr << "Yes" << endl;
    else
        cerr << "No" << endl;
    cerr << "\tIs Dark World? ";
    if(node.isDarkWorld)
        cerr << "Yes" << endl;
    else
        cerr << "No" << endl;
    cerr << "\tChance for Odin: " << node.chanceForOdin << endl;
    cerr << "\tDistance: " << node.distance << endl << "\tStatus: " << node.status<< endl;
    cerr <<"\tBFS Parent: ";
    if(node.parentBFS == NULL)
        cerr << "NULL" << endl;
    else
        cerr << node.parentBFS->name << endl;
}

void printfOneNodeStdOut(tree node, ofstream &fout){
    fout << node.name << endl;
    fout << "\t" << node.strength << " " << node.evil << " " <<node.cure << endl;
    fout << "\t" << node.parentName << " ";
    if(node.leftChild)
        fout << node.leftChildName << " left ";
    if(node.rightChild)
        fout << node.rightChildName << " right ";
    fout << endl;
    fout << "\tOdin, Are you here? ";
    if(node.isOdinHere)
        fout << "Yes" << endl;
    else
        fout << "No" << endl;
    fout << "\tIs Infected? ";
    if(node.isInfectedWorld)
        fout << "Yes" << endl;
    else
        fout << "No" << endl;
    fout << "\tIs Dark World? ";
    if(node.isDarkWorld)
        fout << "Yes" << endl;
    else
        fout << "No" << endl;
    fout << "\tChance for Odin: " << node.chanceForOdin << endl;
    fout << "\tDistance: " << node.distance << endl << "\tStatus: " << node.status<< endl;
    fout <<"\tBFS Parent: ";
    if(node.parentBFS == NULL)
        fout << "NULL" << endl;
    else
        fout << node.parentBFS->name << endl;
}
void printOneWorld(world buffer){
    cerr << buffer.name << endl;
    cerr << buffer.strength << " " << buffer.evil << " " <<buffer.cure << endl;
    cerr << buffer.parentName << " ";
    if(buffer.leftChild)
        cerr << buffer.leftChildName << " left ";
    if(buffer.rightChild)
        cerr << buffer.rightChildName << " right ";
    for(int i = 0; i < 2; i++)
        cerr << endl;
}

vector<world> createArrayOfWorlds(int n, ifstream &fin){
    vector<world> buffer((unsigned long)n);
    bool flag = false;
    for(int i = 0; i < n; i++){
        if(!flag){
            fin >> buffer[i].name;
            fin >> buffer[i].strength;
        }
        fin >> buffer[i].evil >> buffer[i].cure;
        fin >> buffer[i].parentName;
        buffer[i].rightChild = 0;
        buffer[i].leftChild = 0;
        string tmp1, tmp2;
        fin >> tmp1 >> tmp2;
        if(tmp2 == "right"){
            buffer[i].rightChild = 1;
            buffer[i].rightChildName = tmp1;
        }
        else if(tmp2 == "left") {
            buffer[i].leftChild = 1;
            buffer[i].leftChildName = tmp1;
        }
        else{
            buffer[i+1].name =  tmp1;
            flag = 1;
            continue;
        }
        fin >> tmp1 >> tmp2;
        if(tmp2 == "right"){
            buffer[i].rightChild = 1;
            buffer[i].rightChildName = tmp1;
            flag = 0;
        }
        else if(tmp2 == "left") {
            buffer[i].leftChild = 1;
            buffer[i].leftChildName = tmp1;
            flag = 0;
        }
        else{
            buffer[i+1].name = tmp1;
            flag = 1;
            continue;
        }
    }
    return buffer;
}


void createTreeOfNodes(int n, tree *&node, vector<world> buffer, string nodeName, vector<tree*> &member){
    int i = findWorldsNameInBuffer(n, buffer, nodeName);
    node->name = buffer[i].name;
    node->parentName = buffer[i].parentName;
    node->strength = buffer[i].strength;
    node->evil = buffer[i].evil;
    node->cure = buffer[i].cure;
    node->leftChild = buffer[i].leftChild;
    node->rightChild = buffer[i].rightChild;
    node->leftChildName = buffer[i].leftChildName;
    node->rightChildName = buffer[i].rightChildName;
    node->status = 0;
    node->distance = 0;
    node->isInfectedWorld = (node->evil < node->cure ||  (node->cure == node->evil && node->evil == 0) )? false : true;
    node->isDarkWorld = (node->evil > node->strength)? true : false;
    node->isOdinHere = (node->name == "Asgard")? true : false;
    node->isDead = false;
    int neighbors = node->leftChild + node->rightChild;
    neighbors += (node->name == "great_pit")? 0:1;
    node->chanceForOdin =  ((node->evil / 2.0f - (float)node->cure + 1) * (node->distance + 1.0f) * (node->evil + 1.0f) ) / (((float)node->strength / (node->evil + 1) + 1.0f) * (neighbors + 1));
    node->chanceForOdin = (!node->isInfectedWorld)? 0.0f : node->chanceForOdin;
    node->chanceForOdin = (node->name == "Asgard")? node->chanceForOdin * 5.0f : node->chanceForOdin;
    node->chanceForOdin = (node->isDarkWorld)? node->chanceForOdin * 3.0f : node->chanceForOdin;
    node->parentBFS = NULL;
    member.push_back(node);
    if(!node->leftChild && !node->rightChild)
        return;
    if(node->leftChild){
        node->leftNode = new tree;
        node->leftNode->parentNode = node;
        createTreeOfNodes(n, node->leftNode, buffer, node->leftChildName, member);
    }
    if(node->rightChild){
        node->rightNode = new tree;
        node->rightNode->parentNode = node;
        createTreeOfNodes(n, node->rightNode, buffer, node->rightChildName, member);
    }
}

vector<tree*> createBinaryTree(int n, tree* root, ifstream &fin){
    vector<world> buffer = createArrayOfWorlds(n, fin);
    vector<tree*> member;
    createTreeOfNodes(n, root, buffer, "great_pit", member);
    return member;
}

void freeMemory(vector<tree*> member){
    for(int i = 0; i < member.size(); i++){
        cerr << "Deleting " << member[i]->name << endl;
        delete member[i];
    }
}
