//
// Created by Artem Bobrov on 22.01.17.
//

#ifndef LAB4_1_TREE_H
#define LAB4_1_TREE_H
#include "defines.h"

typedef struct worldArray{
    string name, parentName;
    int strength, evil, cure;
    int leftChild, rightChild;
    string leftChildName, rightChildName;
} world;

struct tree {
    string name, parentName;
    int strength, evil, cure;
    int leftChild, rightChild;
    string leftChildName, rightChildName;
    tree *leftNode, *rightNode, *parentNode;
    bool isOdinHere;
    bool isDarkWorld;
    bool isInfectedWorld;
    bool isDead;
    float chanceForOdin;
    int status;
    int distance;
    tree* parentBFS;
};

vector<world> createArrayOfWorlds(int n, ifstream &fin);
int findWorldsNameInBuffer(int n, vector<world> buffer, string name);
void printOneWorld(world buffer);

void printfOneNodeStdErr(tree Node);
void printfOneNodeStdOut(tree node, ofstream &fout);

vector<tree*> createBinaryTree(int n, tree* root, ifstream &fin);
void createTreeOfNodes(int n, tree *&node, vector<world> buffer, string nodeName, vector<tree*> &member);
void freeMemory(vector<tree*> member);
#endif //LAB4_1_TREE_H
