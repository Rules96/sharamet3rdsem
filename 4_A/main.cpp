#include "defines.h"
#include "tree.h"
#include "odin.h"

int main() {
    ifstream fin;
    ofstream fout;
    fin.open("input.txt");
    fout.open("output.txt");
    int n;
    fin >> n;
    tree* root = new tree;
    vector<tree*> member = createBinaryTree(n, root, fin);
    vector<tree*> path;
    for (int i = 0; i < member.size(); i++)
        printfOneNodeStdErr(*member[i]);
    while(true) {
        OdinsStep(root, member, path);
        for (int i = 0; i < member.size(); i++)
            printfOneNodeStdOut(*member[i], fout);
        fout << "-----------------------------NEXT STEP!!!\n";
    }
    freeMemory(member);
    return 0;
}