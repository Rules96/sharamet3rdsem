//
// Created by Artem Bobrov on 22.01.17.
//

#ifndef LAB4_1_ODIN_H
#define LAB4_1_ODIN_H

#include "tree.h"
#include "defines.h"

void BFSInTree(tree* root);
tree* OdinsLocation(vector<tree*> member);
tree* closestInfectedWorld(tree* node,vector<tree*> member);
tree* closestDarkWorld(tree* node, vector<tree*> member);
void spreadInfection(tree* root);
tree* searchInTreeByName(string name, vector<tree*> member);
void initiate(vector<tree*> member);
void spreadInfection(tree* node);
vector<tree*> pathFromTreeToTree(tree* start, tree* finish);
void updateState(tree* node);
void OdinsStep(tree* root, vector<tree*> member, vector<tree*> &path);
bool isGameOver(vector<tree*> member);
void printfOneNodeStdOut(tree node);
#endif //LAB4_1_ODIN_H
