//
// Created by Artem Bobrov on 22.01.17.
//

#ifndef LAB4_1_DEFINES_H
#define LAB4_1_DEFINES_H

#include <string>
#include <fstream>
#include <iostream>
#include <vector>
#include <queue>
#include <stack>

using std::cout;
using std::endl;
using std::cin;
using std::cerr;
using std::fscanf;
using std::ifstream;
using std::ofstream;
using std::vector;
using std::string;
using std::stoi;
using std::queue;
using std::stack;

#endif //LAB4_1_DEFINES_H
