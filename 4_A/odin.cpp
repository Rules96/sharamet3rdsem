//
// Created by Artem Bobrov on 22.01.17.
// #define
#include <climits>
#include "defines.h"
#include "odin.h"
#include "tree.h"

void BFSInTree(tree* root){
    root->status = 1;
    root->distance = 0;
    queue<tree*> q;
    q.push(root);
    while (!q.empty()){
        tree* tmp = q.front();
        q.pop();
        if ( tmp->leftChild && !tmp->leftNode->status) {
            tmp->leftNode->status = 1;
            tmp->leftNode->distance = tmp->distance + 1;
            tmp->leftNode->parentBFS = tmp;
            q.push(tmp->leftNode);
        }
        if ( tmp->rightChild && !tmp->rightNode->status) {
            tmp->rightNode->status = 1;
            tmp->rightNode->distance = tmp->distance + 1;
            tmp->rightNode->parentBFS = tmp;
            q.push(tmp->rightNode);
        }
        if ( tmp->parentNode && !tmp->parentNode->status) {
            tmp->parentNode->status = 1;
            tmp->parentNode->distance = tmp->distance + 1;
            tmp->parentNode->parentBFS = tmp;
            q.push(tmp->parentNode);
        }
    }
}

vector<tree*> pathFromTreeToTree(tree* start, tree* finish){
    vector<tree*> path;
    tree* tmp = finish;
    int k = tmp->distance;
    while (k > 0){
        path.insert( path.begin(), tmp);
        tmp = tmp->parentBFS;
        k--;
    }
    return path;
}


tree* searchInTreeByName(string name, vector<tree*> member) {
    for(int i = 0; i < member.size(); i++){
        if( !member[i]->isDead && member[i]->name == name ){
            return member[i];
        }
    }
    return NULL;
}

void initiate(vector<tree*> member){
    for(int i = 0; i < member.size(); i++){
        if(!member[i]->isDead) {
            member[i]->status = 0;
            member[i]->distance = 0;
            member[i]->parentBFS = NULL;
        }
    }
}

tree* OdinsLocation(vector<tree*> member){
    for(int i = 0; i < member.size(); i++ ){
        if( !member[i]->isDead && member[i]->isOdinHere)
            return member[i];
    }
    return NULL;
}

tree* closestInfectedWorld(tree* node,vector<tree*> member){
    initiate(member);
    BFSInTree(node);
    int Min = INT_MAX;
    tree* tmp = NULL;
    for(int i = 0; i < member.size(); i++){
        if( !member[i]->isDead && member[i]->distance < Min && member[i]->isInfectedWorld ){
            Min = member[i]->distance;
            tmp = member[i];
        }
    }
    return tmp;
}

tree* closestDarkWorld(tree* node, vector<tree*> member){
    initiate(member);
    BFSInTree(node);
    int Min = INT_MAX;
    tree* tmp = NULL;
    for(int i = 0; i < member.size(); i++){
        if( !member[i]->isDead && member[i]->distance < Min && member[i]->isDarkWorld ){
            Min = member[i]->distance;
            tmp = member[i];
        }
    }
    return tmp;
}
// AsgardLocation == searchInTreeByName("Asgard", member);
// distanceFromAsgardToClosestInfectedWorld == closestInfectedWorld( searchInTreeByName("Asgard", member), member);
// distanceFromOdinToClosestInfectedWorld == closestInfectedWorld( OdinsLocation(member), member);

void spreadInfection(tree* node){
    if(node->isDarkWorld){
        int neighbors = node->leftChild + node->rightChild;
        neighbors += (node->name == "great_pit")? 0:1;
        if(node->leftChild)
            node->leftNode->evil += node->evil /  neighbors;
        if(node->rightChild)
            node->rightNode->evil += node->evil / neighbors;
        if(node->name != "great_pit")
            node->parentNode->evil += node->evil / neighbors;
    }
    else if(node->isInfectedWorld){
        int neighbors = node->leftChild + node->rightChild;
        neighbors += (node->name == "great_pit")? 0:1;
        node->evil /= 2;
        if(node->leftChild)
            node->leftNode->evil += node->evil /  neighbors;
        if(node->rightChild)
            node->rightNode->evil += node->evil / neighbors;
        if(node->name != "great_pit")
            node->parentNode->evil += node->evil / neighbors;
    }
    if(node->rightChild)
        spreadInfection(node->rightNode);

    if(node->leftChild)
        spreadInfection(node->leftNode);

}

void updateState(tree* node){

    node->evil = (node->evil <= node->cure )? 0 : node->evil;
    node->isDarkWorld = (node->evil > node->strength)? true : false;
    node->isInfectedWorld = (node->evil < node->cure ||  (node->cure == node->evil && node->evil == 0) )? false : true;
    int neighbors = node->leftChild + node->rightChild;
    neighbors += (node->name == "great_pit")? 0:1;
    node->chanceForOdin =  ((node->evil / 2.0f - (float)node->cure + 1) * (node->distance + 1.0f) * (node->evil + 1.0f) ) / (((float)node->strength / (node->evil + 1) + 1.0f) * (neighbors + 1));
    node->chanceForOdin = (!node->isInfectedWorld)? 0.0f : node->chanceForOdin;
    node->chanceForOdin = (node->name == "Asgard")? node->chanceForOdin * 5.0f : node->chanceForOdin;
    node->chanceForOdin = (node->isDarkWorld)? node->chanceForOdin * 3.0f : node->chanceForOdin;
    if(!node->leftChild && !node->rightChild && node->evil == node->strength && node->isDarkWorld){ // заражение максимально, то есть = силе
        node->isDead = true;
        if(node->parentNode->leftChild && node->parentNode->leftNode->name == node->name)
            node->parentNode->leftChild = 0;
        else
            node->parentNode->rightChild = 0;
    }
    if(node->leftChild)
        updateState(node->leftNode);
    if(node->rightChild)
        updateState(node->rightNode);
}

bool isGameOver(vector<tree*> member){
    bool result = true;
    for (int i = 0; i < member.size(); ++i) {
        if( member[i]->chanceForOdin ){
            result = false;
        }
    }
    if(result) {
        cout << "Odin saves the world\n";
        return true;
    }
    int count = 0;
    for (int i = 0; i < member.size(); ++i) {
        if( member[i]->isInfectedWorld || member[i]->isDead )
            count++;
    }
    if(count == member.size() - 1){
        cout << "Odin loses! Odin is defending Asgard\n";
        return true;
    }
    return false;
}

void OdinsStep(tree* root, vector<tree*> member, vector<tree*> &path){
    updateState(root);
    if(isGameOver(member)){
        cout << "->Game over<-\n";
        freeMemory(member);
        exit(0);
    }
    tree* GodLocation = OdinsLocation(member);
    if(path.size()){
        //dont change his way
        GodLocation->isOdinHere = false;
        path.front()->isOdinHere = true;
        path.erase(path.begin());
        spreadInfection(root);
        return;
    }
    // chooses the way he goes
    initiate(member);
    BFSInTree(GodLocation);
    float Max = 0.0f;
    tree* tmp = NULL;
    for (int i = 0; i < member.size(); ++i) {
        if(Max < member[i]->chanceForOdin){
            Max = member[i]->chanceForOdin;
            tmp = member[i];
        }
    }
    path = pathFromTreeToTree(GodLocation, tmp);
    if(tmp->distance > 0)
        OdinsStep(root, member, path);
}

