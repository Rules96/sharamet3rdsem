/*
������������ ������ �2 
2)�������� � ������ ���������� ��� ������ �� ����� �������� ��������. 
������� ����� ��������� ��� ��������, �� ��� ��� ������ ��������. ��� ������ 
������� ��������� �������� �� ������� ������� �������� �������� �������� ���, 
���� � 
�������� �������� ���, ���� ��� ������ ������ ����� ��� �� ��������� �����. �� 
���-�� 
����� �� ��� ���� ���������, �� ������ �� �����. ��� �����? ������ ������� ��� 
��� 
����� �� ������� ����� ���� � ��������� ������� ������������! ��������� ������� 
���������� ��������� ����� ������� �� ��������� �� ��������, ������ ��������� 
���������� �������, �� ����� ��� � �������� ������������� �����.  
������������ ������� ������, ��� ������ ���������� ����������� �� ������� 
���������� 
S. 
� ���������� ������: ��� ������ � ������� ������. �� ������ �� �������, � 
������������ 
���� ���������. ��� ����������� ��� Y, � ���� ��� X � �������������� ����� ��� 
��������. 
������ �������� ��������� �������, ��������� ���������� �������, ��������� ��� 
� ������ 
��������� ������ �������� ����.  
P.S. ������� ��������� ��������: 
1)sin(x) 
2)x*cos(x) 
3)x^2+x-tg(x) 
4)e^x+3 
������ ������������ ������� �� ����������� S � ������ ��� ����� ����� �������� 
� 
����� ������ �����!� 
������ �������� �����: 
S 
a b 
c 
S - �������, a � b ���������� �� ��� X ��� ������ ������������ ��� Y, � - 
���������� �� 
��� Y ��� ������, ������������ ��� X.
*/
#include <math.h>
#include <stdio.h>    
#include <stdlib.h>
#include <float.h>


double EPS;

double getEps(){
    double e = 1.0;
    while(1.0+e/2.0 > 1.0) e/= 2.0;
    return e;
}

//http://textmechanic.com/text-tools/basic-text-tools/addremove-line-breaks/

double f1(double x){
    return sin(x);
}
double f2(double x){
    return x*cos(x);
}
double f3(double x){
    return x*x+x-tan(x);
}

double f4(double x){
    return exp(x)+3 ;
}

int dmin(double* arr, int n) {
    int i;
    double theMin = DBL_MAX;
    for(i = 0; i < n; i++) {
        if(arr[i] < theMin) theMin = arr[i];
    }
    for(i = 0; i < n; i++) {
        if(((arr[i] - theMin) < EPS) && ((arr[i] - theMin) > -EPS)) {
            i = i;
            break;
        }
    }
    return i;
}

double defIntegral(double (*f)(double),double a, double b, double c){
    double x, res;    
    for(x=a;x<b;x+=10E-4){
        res+=f(x)*x;
    }
    if(c > EPS) {
        res -= (b - a) * c;
    }
    if(c < -EPS) {
        res += (b - a) * (-c);
    }
    return res;
}



int main() {    
    int fn;
    double S, a, b, c;
    double ress[4];
    FILE* inFile = fopen("input.txt", "r");
    const char * const funcNames[] = {"sin(x)", "x*cos(x)", "x^2+x-tg(x)", "e^x+3"};
    fscanf(inFile, "%lf %lf %lf %lf", &S, &a, &b, &c);
    EPS = getEps();
    ress[0] = fabs(defIntegral(f1, a, b, c) - S);
    ress[1] = fabs(defIntegral(f2, a, b, c) - S);
    ress[2] = fabs(defIntegral(f3, a, b, c) - S);
    ress[3] = fabs(defIntegral(f4, a, b, c) - S);
    fn = dmin(ress, 4);
    printf("%s", funcNames[fn]);
    return 0;  
}
