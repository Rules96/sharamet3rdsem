/*
������������ ������ �2
1)��� ��������
���� ���������� ����� �� ������� ������ ���������� ����. ����� ������ �����
����� ��
���������2(������������ � ����������� ������) ��� ������ � ��������� ���
��������������������  ��������, � ������� ������ ������������ ����������
��������� ��
�����. ������ �������, ��� � ������ ���� ����������, ��� ���� ������� �������:
1)����� ��������� ���� ���� ������ � �� �����
2)� ���� ���� ��� �������, ������ ��������� ����.
3)����� �������� � ����� ������� ����.
4)���� ������������� � ������ ������ ����.
5)����� ����� �� ���� ������ ������ ��� �� ���� ����.
6)����� �������� �� ����� ������ ���������� ������������ �� ����� �� 1 �� 10.
��� �����������, ����� ������ ����� �� �������, ������� �� ������� �
�����������
����������� ������������� � �� ������ �� � ������� ������� �������(�� 8 �
����).
P.S. ����������� ������ ���� (10^3)*(10^3). ��,�� ��� ������������
����������������.
�� ������� �����: ������ ����, ������ �������������.
P.S.2 ������� ��������� � ���� ��������� �� ������ ����������, ������ ��
�������
��������� �� ������������� ������.
*/

#include <stdio.h>
#include <stdlib.h>

//#define DEBUG

//MAX_INT = 2^31-1 = 2 147 483 647 > (10^6)*10
#define NO_WAY 2147483647 // As we search for the MINIMUM let this be MAXIMUM..
#define FROM_UPPER 1 //Where do we come here from
#define FROM_LEFT 2

//http://textmechanic.com/text-tools/basic-text-tools/addremove-line-breaks/


void initArr(int * **arr, int n, int m) {
    int i;
    *arr = (int**)malloc(n * sizeof(int*));
    for(i = 0; i < m; i++) {
        (*arr)[i] = (int*)malloc(m * sizeof(int));
    }
}
int getField(int * **field) {
    int n, i, j;
    FILE* inFile = fopen("input.txt", "r");
    fscanf(inFile, "%d", &n);
    initArr(field, n, n);
    for(i = 0; i < n; i++) {
        for(j = 0; j < n; j++) {
            fscanf(inFile, "%d", &((*field)[i][j]));
        }
    }
    return n;
}

void printArr(int **field, int n) {
    int i, j;
    for(i = 0; i < n; i++) {
        for(j = 0; j < n; j++) {
            printf("%d ", field[i][j]);
        }
        printf("\n");
    }
}

void printPath(int **from, int i, int j) {
    if((i == 0) && (j == 0)) {
        printf("0;0\n");
        return;
    } else {
        if(from[i][j] == FROM_UPPER) printPath(from, i - 1, j);
        else printPath(from, i, j - 1);
    }
    printf("%d;%d\n", i, j);
}

void dyn(int** field, int** dp, int** from, int n) {
    int i, j;
    //If there's field[this cell] > 7 => dp[this cell] = NO_WAY;
    dp[0][0] = field[0][0] < 8 ? field[0][0] : NO_WAY;    
    
    for(i = 1; i < n; i++) { //Down first coloumn
        if(field[i][0] > 7) {
            dp[i][0] = NO_WAY;
            continue;
        }
        if(dp[i - 1][0] == NO_WAY) {
            dp[i][0] = NO_WAY;
            continue;
        }
        dp[i][0] = field[i][0] + dp[i - 1][0];
        from[i][0] = FROM_UPPER;
    }
    for(i = 1; i < n; i++) { //Right first row
        if(field[0][i] > 7) {
            dp[0][i] = NO_WAY;
            continue;
        }
        if(dp[0][i - 1] == NO_WAY) {
            dp[0][i] = NO_WAY;
            continue;
        }
        dp[0][i] = field[0][i] + dp[0][i - 1];
        from[0][i] = FROM_LEFT;        
    }
    
    for(i = 1; i < n; i++) {
        for(j = 1; j < n; j++) {
#ifdef DEBUG
            printf("\n");
            printArr(dp, n);
#endif // DEBUG
            if(field[i][j] > 7) {
                dp[i][j] = NO_WAY;
                continue;
            }
            if((dp[i - 1][j] < dp[i][j - 1]) && (dp[i - 1][j] != NO_WAY)) {
                dp[i][j] = field[i][j] + dp[i - 1][j];
                from[i][j] = FROM_UPPER;
#ifdef DEBUG
                printf("FU\n");
#endif // DEBUG
            } else if(dp[i][j - 1] != NO_WAY) {
                dp[i][j] = field[i][j] + dp[i][j - 1];
                from[i][j] = FROM_LEFT;
#ifdef DEBUG
                printf("FL\n");
#endif // DEBUG            
            } else  dp[i][j] = NO_WAY;
        }
    }
}

/*void generateField(int n){
    int i,j;
    FILE* outFIle = fopen("input.txt","r");
    
}
*/

int main() {    
    int n;
    int **dp; //MAX_INT = 2^31-1 = 2 147 483 647 > (10^6)*10 => enough
    int **field;
    int **from;
    //
    n = getField(&field);
    initArr(&dp, n, n);
    initArr(&from, n, n);  
    dyn(field, dp, from, n);
    //generateField(1000);
#ifdef DEBUG
    printf("%d\n\n", dp[n - 1][n - 1]);
#endif // DEBUG
    if(dp[n - 1][n - 1] != NO_WAY)  printPath(from, n - 1, n - 1);
    else printf("No way(");
    return 0;
}
