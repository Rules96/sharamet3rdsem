/*
2)������������������
������� ��� ��������� �� n-�� �������.
��� ���������� ������� ������������ ������, ����������� ��� F[n], ��� n - ���������.
F(0)=0, F(1) =1, F(n)=F(n-1)+F(n-2)
*/

#include <stdio.h>    
#include <stdlib.h>

//http://textmechanic.com/text-tools/basic-text-tools/addremove-line-breaks/


int main() {
    int* fibs;
    int n, i;
    //
    //freopen("in.txt", "r", stdin);
    //freopen("out.txt", "w", stdout); freopen("out.txt", "w", stdout);
    //
    scanf("%d", &n);
    fibs=(int*)malloc((n+1)*sizeof(int));
    fibs[0]=fibs[1]=1;
    for(i=2;i<n;i++){
        fibs[i]=fibs[i-1]+fibs[i-2];   
    }
    for(i=0;i<n;i++){
        printf("%d\n",fibs[i]);    
    }
    return 0;
}
