#include "_list.h"

//List: front = NULL<--HEAD<-->el<-->el<-->TAIL-->NULL

/**
    * 
*/
void l_construct(_list* *dList, void* headData, void* tailData, Tfree ufree, Tcmpf cmpf){
    l_node* HEAD = (l_node*)malloc(sizeof(l_node));
    l_node* TAIL = (l_node*)malloc(sizeof(l_node));
    *dList = (_list*)malloc(sizeof(_list));    
    HEAD->next=TAIL;
    HEAD->previous=NULL;
    TAIL->next=NULL;
    TAIL->previous=HEAD;
    HEAD->data=headData;
    TAIL->data=tailData;
    (*dList)->HEAD = HEAD;
    (*dList)->TAIL = TAIL;
    (*dList)->ufree = ufree;
    (*dList)->cmpf = cmpf;    
}


/**
    * Inserts a newNode after node
    * returns *newNode
*/
l_node* l_insertAfter(l_node* node, void* data){
    l_node* newNode = (l_node*)malloc(sizeof(l_node));
    newNode -> data = data;
    newNode -> next = node -> next ?  node -> next -> next : NULL;
    node -> next = newNode;
    newNode->previous = node;
    return newNode;
}

/**
    * Inserts a newNode before node
    * returns *newNode
*/
l_node* l_insertBefore(l_node* node, void* data){
    l_node* newNode = (l_node*)malloc(sizeof(l_node));
    newNode -> data = data;
    newNode -> previous = node -> previous;
    if(node -> previous) node -> previous  -> next = newNode;
    node -> previous = newNode;
    newNode->next = node;
    return newNode;
}

/**
    * Calls Tfree function for non-void *data
    * DO NOT USE IF toRemove doesn't belong to flist        
*/
void l_remove( _list*  dList, l_node* toRemove){           
    if(toRemove==dList->HEAD){        
        dList->HEAD = toRemove->next;
        dList->HEAD -> previous = NULL; 
        if(toRemove->data) dList->ufree(toRemove->data);   
        free(toRemove);             
    }    
    else if(toRemove==dList->TAIL){
        dList->TAIL = dList->TAIL->previous;                     
        dList->TAIL->next=NULL;
        if(toRemove) dList->ufree(toRemove);
        if(toRemove->data) dList->ufree(toRemove->data);
        free(toRemove);            
    } else{        
        toRemove->previous->next = toRemove->next;
        toRemove->next->previous = toRemove->previous;
        if(toRemove) dList->ufree(toRemove);
        if(toRemove->data) dList->ufree(toRemove->data);           
        free(toRemove);            
    }           
}

/**
    * 
*/
l_node* l_pushFront(_list* dlist, void* data){    
    return  dlist->HEAD = l_insertBefore(dlist->HEAD,data);   
}

/**
    * Removes HEAD and returns it's *data
*/
void* l_popFront(_list* dList){    
    return l_remove(dList,dList->HEAD);   
}

/**
    * 
*/
l_node* l_pushBack(_list* dList, void* data){
    return dList->TAIL = l_insertAfter(dList->TAIL, data);       
}

/**    
    * Removes TAIL and returns it's *data    
*/
void* l_popBack(_list* dList){
    return l_remove(dList,dList->TAIL);
}

/**
    * Returns l_node* or flist -> HEAD -> next
    * (which is NULL by default)
*/
l_node* l_search(_list*  dList, Tsrchf srchf){
    l_node* curr;                            //Assuming flist->TAIL->next=NULL
    for(curr = dList -> HEAD;curr;curr = curr->next){ //curr!=flist->TAIL->next
        if(srchf(curr->data)) break;    
    }
    return curr;
}


/**
    *  Bubble sort    
*/
void l_sort(_list*  dList, Tcmpf cmpf){
    l_node *i, *j;
    void* tmp;
    for(j=dList->HEAD;j->next;j=j->next){  
        for(i=dList->HEAD;i->next;i=i->next){
            if(cmpf(i->data, i->next->data)>0){
                tmp = i->data;
                i->data = i->next->data;
                i->next->data = tmp;           
            }  
        }
    }
   
}

/** 
    * http://pastebin.com/EVwuTAiF
    * Merging lists ending in next == 0,
    * according to fcmp(elem1, elem2) (returns N < 0, 0 for EQUAL or N > 0)
    * l1, l2 are pointers to lists' first elements
    * prev pointers become useless!!! TODO: FIX? 
 */
l_node* merge2lists (l_node* l1, l_node* l2, Tcmpf fcmp){
  l_node result = {NULL, NULL, NULL};
  l_node *cur = &result;
 
  while (l1 && l2)
    if (fcmp(l1->data, l2->data) <= 0) {
      cur->next = l1; cur = l1; l1 = l1->next;
    } else {
      cur->next = l2; cur = l2; l2 = l2->next;
    }
  cur->next = (l1 ? l1 : l2); // ���������� �������� ������� � ����� result
  return result.next;
}

/**
    * Sort list using a pointer to it's first element
    * according to fcmp(elem1, elem2) (returns N < 0, 0 for EQUAL or N > 0)
    * prev pointers become useless!!! TODO: FIX?
*/
l_node* l_mergesort (l_node *list, Tcmpf fcmp){    
    if (list == 0 || list->next == 0) return list;
    l_node *a = list, *b = list->next;
    // Cunningly split the list into two. b is loacated an element after.
    while (b && b->next) {
        list = list->next; b = b->next->next;
    }
    b = list->next;
    list->next = 0; // split, terminate first part

    return merge2lists(l_mergesort(a, fcmp), l_mergesort(b, fcmp), fcmp);
}

void l_QSort(_list *l, Tcmpf cmpf){
    l->HEAD = l_mergesort(l->HEAD, cmpf);
}  

  
void l_free(_list*  *dList){
    l_node* tmp = (*dList)->HEAD;
    l_node* tmp2 = tmp;
    while(tmp2){
        tmp2 = tmp->next;  
        dList->ufree(tmp -> data);  
        free(tmp);
        tmp = tmp2;        
    }
    free(*dList);    
}
