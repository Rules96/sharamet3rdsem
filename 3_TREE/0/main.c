#include <stdio.h>
#include "_forward_list.h" //single linked list
#include "_list.h" //doubly linked list

#define LVoid(val) (void*)&(val)
#define Int(val) (*((int*)(val)))

void fl_print(const _forward_list* flist){
    fl_node* curr;
    printf("The list now is: HEAD: ");//Assuming flist->TAIL->next=NULL
    for(curr=flist->HEAD;curr->next;curr=curr->next){ 
        printf("%d ",Int(curr->data));        
    }
    printf("TAIL: %d\n",Int(curr->data));    
}

void l_print(const _list* list){
    l_node* curr;
    printf("The list now is: HEAD: ");//Assuming list->TAIL->next=NULL
    for(curr=list->HEAD;curr->next;curr=curr->next){ 
        printf("%d ",Int(curr->data));        
    }
    printf("TAIL: %d\n",Int(curr->data));    
}

void ufree(const void* a){
    //None to do since int is not a pointer    
}

int srchf3(const void* a){return Int(a)==3;}
int srchf5(const void* a){return Int(a)==5;}
int srchf2(const void* a){return Int(a)==2;}
int srchf6(const void* a){return Int(a)==6;}

int cmpf(const void* a,const void* b){
    if(a==NULL){
        if(b==NULL) return 0;
             return -1;
    }
    if(b==NULL)return 1;
    return Int(a) - Int(b);
}

int main(int argc, const char* argv[]){ 
    int arr[8] = {1,2,3,4,5,6,7,8};
       
    //====================FORWARD LIST===========================//  
    printf("//====================Forward List:====================//\n");
    _forward_list* myFList;
    fl_construct(&myFList, LVoid(arr[3]),LVoid(arr[4]));
    //--------PUSH FRONT:--//
    fl_print(myFList);
    fl_pushFront(myFList,LVoid(arr[2])); //WHY THE HECK?? COMPLILLER 
    fl_pushFront(myFList,LVoid(arr[1])); //ALLOWED ME TO CALL
    fl_pushFront(myFList,LVoid(arr[0])); //fl_pushFront(myFList,LVoid(arr[2]));
    fl_print(myFList);//if fl_node* fl_pushFront(void* data,  _forward_list* flist);
    //--------POPFRONT:-----//
    printf("---------------PopFront'ed:-----\n");
    printf("%d ",Int(fl_popFront(myFList))); 
    printf("%d ",Int(fl_popFront(myFList)));
    printf("\n"); 
    printf("After: ");fl_print(myFList);  
    //-----INSERT AFTER:---//
    printf("---------------Inserted after TAIL (PushBack...):------\n");
    printf("%d ",Int((myFList->TAIL = fl_insertAfter(myFList->TAIL,LVoid(arr[0])))->data));
    printf("%d ",Int((myFList->TAIL = fl_insertAfter(myFList->TAIL,LVoid(arr[1])))->data));
    printf("\n");
    printf("After: ");fl_print(myFList);
    //--------SEARCH:------//
    printf("-----------Search:-------------\n");
    printf("3?=%d\n", Int((fl_search(myFList,srchf3))->data));
    printf("5?=%d\n", Int((fl_search(myFList,srchf5))->data));
    printf("NULL?=%d\n", (int)fl_search(myFList,srchf6));
    printf("2?=%d\n", Int((fl_search(myFList,srchf2))->data));
    //--------REMOVE NEXT:------//
    printf("--------REMOVE NEXT:--------\n");     
    printf("fl_rmN(HEAD): %d\n", fl_removeNext(myFList->HEAD,ufree));
    printf("fl_rmN(TAIL): %d\n", fl_removeNext(myFList->TAIL,ufree));
    printf("After: ");fl_print(myFList);
    //--------SEARCH REMOVE:------//
    printf("--------SEARCH REMOVE:--------\n"); 
    printf("...(.., srchf5,..): %d\n",fl_searchRemove(myFList, srchf5, ufree));
    ;
    printf("After: ");fl_print(myFList);
    //--------SORT:-------//    
    fl_free(&myFList, ufree);
    fl_construct(&myFList, LVoid(arr[7]),LVoid(arr[6]));       
    myFList->TAIL = fl_insertAfter(myFList->TAIL,LVoid(arr[5]));
    myFList->TAIL = fl_insertAfter(myFList->TAIL,LVoid(arr[4]));
    myFList->TAIL = fl_insertAfter(myFList->TAIL,LVoid(arr[3]));
    myFList->TAIL = fl_insertAfter(myFList->TAIL,LVoid(arr[2]));
    myFList->TAIL = fl_insertAfter(myFList->TAIL,LVoid(arr[1]));
    myFList->TAIL = fl_insertAfter(myFList->TAIL,LVoid(arr[0]));
    printf("Before: ");fl_print(myFList);
    printf("--------FAIL Sort:--------\n");     
    _fl_FAIL_sort(myFList,cmpf);
    printf("After: ");fl_print(myFList);
    printf("--------Sort:--------\n");     
    fl_sort(myFList,cmpf);
    printf("After: ");fl_print(myFList);
    
    
    //--------QSORT:-------//    
    fl_free(&myFList, ufree);
    fl_construct(&myFList, LVoid(arr[7]),LVoid(arr[6]));       
    myFList->TAIL = fl_insertAfter(myFList->TAIL,LVoid(arr[5]));
    myFList->TAIL = fl_insertAfter(myFList->TAIL,LVoid(arr[4]));
    myFList->TAIL = fl_insertAfter(myFList->TAIL,LVoid(arr[3]));
    myFList->TAIL = fl_insertAfter(myFList->TAIL,LVoid(arr[2]));
    myFList->TAIL = fl_insertAfter(myFList->TAIL,LVoid(arr[1]));
    myFList->TAIL = fl_insertAfter(myFList->TAIL,LVoid(arr[0]));        
    printf("--------QSort:--------\n");
    printf("Before: ");fl_print(myFList);     
    fl_QSort(myFList, cmpf);
    printf("After: ");fl_print(myFList);
    
    // Turns out that free(pointer) does not set that pointer to NULL.
    // If you want that to be the case, you have to set it yourself,
    // and it is a common idiom in C:
    //http://stackoverflow.com/questions/7774443/
    fl_free(&myFList, ufree);
    myFList = NULL;
    
    //===========================LIST============================//
    printf("\n\n//====================Doubly-Linked List:====================//\n");
    _list* myList;
    l_construct(&myList, LVoid(arr[3]),LVoid(arr[4]));
    //--------PUSH FRONT:--//
    l_print(myList);
    l_pushFront(myList,LVoid(arr[2])); 
    l_pushFront(myList,LVoid(arr[1])); 
    l_pushFront(myList,LVoid(arr[0])); 
    l_print(myList);
    //--------POPFRONT:-----//
    printf("---------------PopFront'ed:-----\n");
    printf("%d ",Int(l_popFront(myList))); 
    printf("%d ",Int(l_popFront(myList)));
    printf("\n"); 
    printf("After: ");l_print(myList);  
    //-----INSERT AFTER:---//
    printf("---------------Inserted after TAIL :------\n");
    printf("%d ",Int((myList->TAIL = l_insertAfter(myList->TAIL,LVoid(arr[0])))->data));
    printf("%d ",Int((myList->TAIL = l_insertAfter(myList->TAIL,LVoid(arr[1])))->data));
    printf("\n");
    printf("After: ");l_print(myList);
    //-----INSERT BEFORE:---//
    printf("---------------Inserted before TAIL :------\n");
    printf("%d ",Int((l_insertBefore(myList->TAIL,LVoid(arr[0])))->data));
    printf("%d ",Int((l_insertBefore(myList->TAIL,LVoid(arr[1])))->data));
    printf("\n");
    printf("After: ");l_print(myList);
    //--------SEARCH:------//
        

    //--------SEARCH REMOVE:------//
    printf("--------SEARCH REMOVE:--------\n"); 
    printf("l_rem(..l_srch(5)): %d\n",Int(l_remove(myList, l_search(myList, srchf5))));
    printf("After: ");l_print(myList);
    //--------SORT:-------//
    printf("--------Sort:--------\n");       
    l_free(&myList, ufree);
    l_construct(&myList, LVoid(arr[7]),LVoid(arr[6]));       
    myList->TAIL = l_insertAfter(myList->TAIL,LVoid(arr[5]));
    myList->TAIL = l_insertAfter(myList->TAIL,LVoid(arr[4]));
    myList->TAIL = l_insertAfter(myList->TAIL,LVoid(arr[3]));
    myList->TAIL = l_insertAfter(myList->TAIL,LVoid(arr[2]));
    myList->TAIL = l_insertAfter(myList->TAIL,LVoid(arr[1]));
    myList->TAIL = l_insertAfter(myList->TAIL,LVoid(arr[0]));
    printf("Before: ");l_print(myList); 
    l_sort(myList,cmpf);
    printf("After: ");l_print(myList);
    
    //--------QSORT:-------//
    printf("--------QSort:--------\n");       
    l_free(&myList, ufree);
    l_construct(&myList, LVoid(arr[7]),LVoid(arr[6]));       
    myList->TAIL = l_insertAfter(myList->TAIL,LVoid(arr[5]));
    myList->TAIL = l_insertAfter(myList->TAIL,LVoid(arr[4]));
    myList->TAIL = l_insertAfter(myList->TAIL,LVoid(arr[3]));
    myList->TAIL = l_insertAfter(myList->TAIL,LVoid(arr[2]));
    myList->TAIL = l_insertAfter(myList->TAIL,LVoid(arr[1]));
    myList->TAIL = l_insertAfter(myList->TAIL,LVoid(arr[0]));
    printf("Before: ");l_print(myList); 
    l_QSort(myList,cmpf);
    printf("After: ");l_print(myList);
    
    l_free(&myList, ufree);
    myList = NULL;     
    
    return 0;
}
