#include "_forward_list.h"

//Forward List: front = HEAD->el->el->TAIL-->NULL

/**
    * 
*/
void fl_construct(_forward_list* *flist, void* headData, void* tailData){
    fl_node* HEAD = (fl_node*)malloc(sizeof(fl_node));
    fl_node* TAIL = (fl_node*)malloc(sizeof(fl_node));
    *flist = (_forward_list*)malloc(sizeof(_forward_list));    
    HEAD->next=TAIL;
    TAIL->next=NULL;
    HEAD->data=headData;
    TAIL->data=tailData;
    (*flist)->HEAD = HEAD;
    (*flist)->TAIL = TAIL;     
}


/**
    * Inserts a newNode after node
    * returns *newNode
*/
fl_node* fl_insertAfter(fl_node* node, void* data){
    fl_node* newNode = (fl_node*)malloc(sizeof(fl_node));
    newNode -> data = data;
    newNode -> next = node -> next ?  node -> next -> next : NULL;
    node -> next = newNode;
    return newNode;
}

/**
    * 
*/
fl_node* fl_pushFront(_forward_list* flist, void* data){
    fl_node* newNode = (fl_node*)malloc(sizeof(fl_node));
    newNode -> data = data;
    newNode -> next = flist->HEAD;
    flist -> HEAD  = newNode;
    return newNode;   
}

/**
    * Removes HEAD and returns it's *data
*/
void* fl_popFront(_forward_list* flist){
    fl_node* toRemove =  flist->HEAD;
    void* data = toRemove->data;
    flist->HEAD = toRemove->next;    
    free(toRemove);
    return data;   
}

/**
    * should NOT be used/public
    * Removes TAIL and returns it's *data
    * ??Lacks flist->BEFORE_TAIL??
*/
void* _fl_popBack(_forward_list* flist){
    fl_node* prev;                  //Assuming flist->TAIL->next=NULL
    void* data = flist->TAIL->data;// prev->next!=flist->TAIL
    for(prev=flist->HEAD;prev->next->next;prev=prev->next);        
    flist->TAIL = prev;
    free(flist->TAIL);
    return data;
}

/**
    * returns 0 if there was anything removed
*/
int fl_removeNext(fl_node*  node, Tfree ufree){
    if (node -> next == NULL) return 1; // None to remove since it's the TAIL!
    fl_node* toRemove = node -> next;    
    node -> next = node -> next -> next;
    ufree(toRemove->data);
    free(toRemove);
    return 0; 
}

/**    
    * Returns fl_node* or flist -> HEAD -> next
    * (which is NULL by default)
*/
fl_node* fl_search(_forward_list*  flist, Tsrchf srchf){
    fl_node* curr;                            //Assuming flist->TAIL->next=NULL
    for(curr = flist -> HEAD;curr;curr = curr->next){ //curr!=flist->TAIL->next
        if(srchf(curr->data)) break;    
    }
    return curr;
}

/**
    * Should NOT be used as fl_remove(fl_search(...)    
    * use fl_searchRemove(flist,srchf,ufree) instead  
    * Returns 1 if removed from HEAD 
    * 2 if removed from TAIL
    * -1 if toRemove doesn't belong to flist
    * 0 on success 
*/
int fl_remove( _forward_list*  flist, fl_node* toRemove, Tfree ufree){
    fl_node* tmp;
    if(toRemove==flist->HEAD){        
        ufree(fl_popFront(flist));
        return 1;     
    }    
    if(toRemove==flist->TAIL){
        ufree(_fl_popBack(flist));
        return 2;
    }
    for(tmp=flist->HEAD;tmp->next;tmp=tmp->next){
        if(tmp->next == toRemove){
            fl_removeNext(tmp, ufree);
            return 0;
        }
    }
    return -1;        
}

/**      
    * -1 if not found
    * 0 on success  
*/
int fl_searchRemove( _forward_list*  flist, Tsrchf srchf, Tfree ufree){
    fl_node* tmp;
    if(srchf(flist->HEAD)){        
        ufree(fl_popFront(flist));
        return 0;     
    }    
    if(srchf(flist->TAIL)){
        ufree(_fl_popBack(flist));
        return 0;
    }
    for(tmp=flist->HEAD;tmp->next;tmp=tmp->next){
        if(srchf(tmp->next->data)){
            fl_removeNext(tmp, ufree);
            return 0;
        }
    }
    return -1;   
}

/** TODO: TO BE DONE
    *  Should NOT be used/public
    *  Ascending sort
    *  Sorts list containing MINIMUM 4 ELEMENTS
    *  Sorts by groups of THREE elments AT ONCE    
*/
void _fl_FAIL_sort(_forward_list*  flist, Tcmpf cmpf){
    fl_node* curr;
    fl_node* tmp;
    fl_node* tmp2;     
    if(flist -> HEAD -> next&&flist -> HEAD -> next -> next&&
       flist -> HEAD -> next -> next -> next){
        curr = flist -> HEAD;
        while(curr->next->next->next){ //Assuming flist->TAIL->next=NULL        
            if(cmpf(curr->next->data, curr->next->next->data)>0){
              tmp = curr->next;
              curr->next = curr->next->next;
              tmp2 = tmp->next->next;   
              curr->next->next = tmp;
              tmp->next = tmp2;              
            } 
            curr=curr->next;            
        }
    }
}

/**
    *  Bubble sort    
*/
void fl_sort(_forward_list*  flist, Tcmpf cmpf){
    fl_node *i, *j;
    void* tmp;
    for(j=flist->HEAD;j->next;j=j->next){  
        for(i=flist->HEAD;i->next;i=i->next){
            if(cmpf(i->data, i->next->data)>0){
                tmp = i->data;
                i->data = i->next->data;
                i->next->data = tmp;           
            }  
        }
    }
   
}

/**
    * ������� �������, ������������� next == 0,
    * � ������������ � fcmp(elem1, elem2) (returns N < 0, 0 for EQUAL or N > 0)
    * l1, l2 ��������� �� ������ �������� �������
    * ��������� prev ���������� �������������
 */
fl_node* merge2flists (fl_node* l1, fl_node* l2, Tcmpf fcmp){
  fl_node result = {NULL, NULL, NULL};
  fl_node *cur = &result;
 
  while (l1 && l2)
    if (fcmp(l1->data, l2->data) <= 0) {
      cur->next = l1; cur = l1; l1 = l1->next;
    } else {
      cur->next = l2; cur = l2; l2 = l2->next;
    }
  cur->next = (l1 ? l1 : l2); // ���������� �������� ������� � ����� result
  return result.next;
}

 /**
    * ���������� ������ �� ��������� �� ��� ������ �������
    * � ������������ � fcmp(elem1, elem2) (returns N < 0, 0 for EQUAL or N > 0)
    * ��������� prev ���������� �������������
 */
fl_node* fl_mergesort (fl_node *list, Tcmpf fcmp){    
    if (list == 0 || list->next == 0) return list;
    fl_node *a = list, *b = list->next;
    // ����� ����� ������ �������. b ����� ����� �������
    while (b && b->next) {
        list = list->next; b = b->next->next;
    }
    b = list->next;
    list->next = 0; // ��������, terminate ������ ��������

    return merge2flists(fl_mergesort(a, fcmp), fl_mergesort(b, fcmp), fcmp);
}

void fl_QSort(_forward_list *l, Tcmpf cmpf){
    l->HEAD = fl_mergesort(l->HEAD, cmpf);
}

/**
TODO FIX!!!!    
*/
void fl_free(_forward_list*  *flist, Tfree ufree){
    fl_node* tmp = (*flist)->HEAD;
    fl_node* tmp2;
    do{
        tmp2 = tmp->next;  
        ufree(tmp -> data);  
        free(tmp);
        tmp = tmp2;
    } while(tmp2->next);              
    free(*flist);    
}

