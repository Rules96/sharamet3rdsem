#ifndef _LIST_H_INCLUDED
#define _LIST_H_INCLUDED
#include <stdlib.h>
//List: front = NULL<--HEAD<-->el<-->el<-->TAIL-->NULL

#ifndef _FORWARD_LIST_H_INCLUDED
typedef int (*Tcmpf)(const void* a,const void* b);

//Should return TRUE when a is what we were searching for:
//and 0 otherwise
typedef int (*Tsrchf)(const void* a);

//Should free memory pointed-to by a as user wants it
typedef void (*Tfree) (const void* a);

#endif // _FORWARD_LIST_H_INCLUDED


typedef struct _l_node {
    void* data;
    struct _l_node* next;
    struct _l_node* previous;    
} l_node;

typedef struct __list {
   l_node* HEAD;
   l_node* TAIL;
   Tfree ufree;
   Tcmpf cmpf;   
} _list;

/**
    * 
*/
void l_construct(_list* *dList, void* headData, void* tailData);


/**
    * Inserts a newNode after node
    * returns *newNode
*/
l_node* l_insertAfter(l_node* node, void* data);

/**
    * Inserts a newNode before node
    * returns *newNode
*/
l_node* l_insertBefore(l_node* node, void* data);

/**
    * DO NOT USE IF toRemove doesn't belong to flist 
    * Returns *data of the removed element       
*/
void* l_remove( _list*  dList, l_node* toRemove);

/**
    * 
*/
l_node* l_pushFront(_list* dlist, void* data);

/**
    * Removes HEAD and returns it's *data
*/
void* l_popFront(_list* dList);

/**
    * 
*/
l_node* l_pushBack(_list* dList, void* data);

/**    
    * Removes TAIL and returns it's *data    
*/
void* l_popBack(_list* dList);

/**
    * Returns l_node* or flist -> HEAD -> next
    * (which is NULL by default)
*/
l_node* l_search(_list*  dList, Tsrchf srchf);


/**
    *  Bubble sort    
*/
void l_sort(_list*  dList, Tcmpf cmpf);

/** 
    * http://pastebin.com/EVwuTAiF
    * Merging lists ending in next == 0,
    * according to fcmp(elem1, elem2) (returns N < 0, 0 for EQUAL or N > 0)
    * l1, l2 are pointers to lists' first elements
    * prev pointers become useless!!! TODO: FIX? 
 */
l_node* merge2lists (l_node* l1, l_node* l2, Tcmpf fcmp);

/**
    * Sort list using a pointer to it's first element
    * according to fcmp(elem1, elem2) (returns N < 0, 0 for EQUAL or N > 0)
    * prev pointers become useless!!! TODO: FIX?
*/
l_node* l_mergesort (l_node *list, Tcmpf fcmp); 

void l_QSort(_list *l, Tcmpf cmpf);
  
void l_free(_list*  *dList, Tfree ufree);


#endif // _LIST_H_INCLUDED
