#ifndef _FORWARD_LIST_H_INCLUDED
#define _FORWARD_LIST_H_INCLUDED
#include <stdlib.h>

//Forward List: front = HEAD->el->el->TAIL-->NULL

#ifndef _LIST_H_INCLUDED
typedef int (*Tcmpf)(const void* a,const void* b);

//Should return TRUE when a is what we were searching for:
//and 0 otherwise
typedef int (*Tsrchf)(const void* a);

//Should free memory pointed-to by a as user wants it
typedef void (*Tfree) (const void* a);

#endif // _LIST_H_INCLUDED


typedef struct _fl_node {
    void* data;
    struct _fl_node* next;    
} fl_node;

typedef struct __forward_list {
   fl_node* HEAD;
   fl_node* TAIL;  
   Tfree ufree;
   Tcmpf cmpf; 
} _forward_list;

/**
    * 
*/
void fl_construct(_forward_list* *flist, void* headData, void* tailData, Tfree ufree);


/**
    * Inserts a newNode after node
    * returns *newNode
*/
fl_node* fl_insertAfter(fl_node* node, void* data);

/**
    * 
*/
fl_node* fl_pushFront(_forward_list* flist, void* data);

/**
    * Removes HEAD and returns it's *data
*/
void* fl_popFront(_forward_list* flist);

/**
    * should NOT be used/public
    * Removes TAIL and returns it's *data
    * ??Lacks flist->BEFORE_TAIL??
*/
void* _fl_popBack(_forward_list* flist);

/**
    * returns 0 if there was anything removed
*/
int fl_removeNext(fl_node*  node);

/**    
    * Returns fl_node* or flist -> HEAD -> next
    * (which is NULL by default)
*/
fl_node* fl_search(_forward_list*  flist, Tsrchf srchf);

/**
    * Should NOT be used as fl_remove(fl_search(...)    
    * use fl_searchRemove(flist,srchf,ufree) instead  
    * Returns 1 if removed from HEAD 
    * 2 if removed from TAIL
    * -1 if toRemove doesn't belong to flist
    * 0 on success 
*/
int fl_remove( _forward_list*  flist, fl_node* toRemove);

/**      
    * -1 if not found
    * 0 on success  
*/
int fl_searchRemove( _forward_list*  flist, Tsrchf srchf);

/** TODO: TO BE DONE
    *  Should NOT be used/public
    *  Ascending sort
    *  Sorts list containing MINIMUM 4 ELEMENTS
    *  Sorts by groups of THREE elments AT ONCE    
*/
void _fl_FAIL_sort(_forward_list*  flist, Tcmpf cmpf);


/**
    *  Bubble sort    
*/
void fl_sort(_forward_list*  flist, Tcmpf cmpf);


/** 
    * http://pastebin.com/EVwuTAiF
    * Merging lists ending in next == 0,
    * according to fcmp(elem1, elem2) (returns N < 0, 0 for EQUAL or N > 0)
    * l1, l2 are pointers to lists' first elements
    * prev pointers become useless!!! TODO: FIX? 
 */
fl_node* merge2flists (fl_node* l1, fl_node* l2, Tcmpf fcmp);

/**
    * Sort list using a pointer to it's first element
    * according to fcmp(elem1, elem2) (returns N < 0, 0 for EQUAL or N > 0)
    * prev pointers become useless!!! TODO: FIX?
*/
fl_node* fl_mergesort (fl_node *list, Tcmpf fcmp);

void fl_QSort(_forward_list *l, Tcmpf cmpf);

/**
TODO FIX!!!!    
*/
void fl_free(_forward_list*  *flist);
#endif // _FORWARD_LIST_H_INCLUDED
