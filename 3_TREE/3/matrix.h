#ifndef MATRIX_H_INCLUDED
#define MATRIX_H_INCLUDED
#include<stdlib.h>
#include<string.h>

/**
    * Vector's elements by default point to NULL;
    * Matrix's rows by default point to NULL;
    * User shold define and pass his destryf destroy_function 
    * to be stored in vector which will be called before freeing any void* ptr 
    * so that user could free the *ptr if  ptr was
    * a pinter to some allocated memory   
**/

typedef void (*Tdestroyf)(const void* ptr);

typedef int (*Tcomparef)(const void* a, const void* b);

//TODO: Almost impossible to implement :((
//Just a stub. Not implemented yet. 
typedef int (*TcmpfWrapper)(const void** a, const void** b); 

typedef struct __vector {
   void* *arr;
   TcmpfWrapper cmpfWrapper; // Wraps cmpf so that user's cmpf could get void*, 
   Tcomparef cmpf;         // not void**; TODO: Almost impossible to implement((
   Tdestroyf destrf;
   int length;
} _vector;

typedef struct __matrix {
   _vector * *rows;
   TcmpfWrapper cmpfWrapper;  // TODO: Does it worth it to duplicate it here
   Tcomparef cmpf;           // so that no to pass it as a parametr to  m_put?
   Tdestroyf destrf;         //The same dilemma..
   int width;
   int height;
} _matrix;

void v_destroy(_vector *v);

void m_destroy(_matrix *m);

void v_init(_vector * *v, int length, Tcomparef cmpf, Tdestroyf destrf);

void m_init(_matrix * *m, _vector *v);

void v_put(_vector *v, int index, void* data);

void m_put(_matrix* m, int i, int j, void* data);

void m_addRow(_matrix *m, int index, _vector* v);

void m_addColumn(_matrix *m, int index, _vector* v);

void v_sort(_vector *v);

void* m_get(_matrix* m, int i, int j);

#endif // MATRIX_H_INCLUDED
