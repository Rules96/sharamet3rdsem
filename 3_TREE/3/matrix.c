#include "matrix.h"

/**
    * Vector's elements by default point to NULL;
    * Matrix's rows by default point to NULL;
    * User shold define and pass his destryf destroy_function 
    * to be stored in vector which will be called before freeing any void* ptr 
    * so that user could free the *ptr if  ptr was
    * a pinter to some allocated memory   
**/

/**
    * DON'T CALL V_DESTROY FOR UNDEFINED _VECTOR*
**/
void v_destroy(_vector *v) {
   int i = v->length;
   while(i--) { //Free non-NULL pointers
      if(v->arr[i]) v->destrf(v->arr[i]);
   }
   free(v->arr);
   free(v);
}

void m_destroy(_matrix *m) {
   int i = m->height;
   while(i--) {
      if(m->rows[i]) v_destroy(m->rows[i]); //Free non-NULL pointers
   }
   free(m->rows);
   free(m);
}


void v_init(_vector * *v, int length, Tcomparef cmpf, Tdestroyf destrf) {
   *v = (_vector*)malloc(sizeof(_vector));
   (*v)->length = length;
   (*v)->cmpf = cmpf;   
   (*v)->destrf = destrf;
   (*v)->arr = (void**)malloc(sizeof(void*)*length); 
   //TODO: check whether NULL really == (void*)0: 
   memset((*v)->arr, 0, sizeof(void*)*length); //Fill the vector
}

void m_init(_matrix * *m, _vector *v) {
   *m = (_matrix*)malloc(sizeof(_matrix));
   (*m)->width = v->length;
   (*m)->height = 1;
   (*m)->rows = (_vector**)malloc(sizeof(_vector*));
   (*m)->rows[0] = v;
   (*m)->cmpf = v->cmpf;
   (*m)->destrf = v->destrf;
}

void v_put(_vector *v, int index, void* data) {
   if(index < 0) return;
   if(index >= v->length) {
      v->arr = (void**)realloc(v->arr, sizeof(void*) * (index + 1));
      memset(v->arr + v->length, 0, sizeof(void*) * (index - v->length)); //Fill inner elements
      v->length = index + 1;
   }
   if(v->arr[index]) v->destrf(v->arr[index]); //Free previous val
   v->arr[index] = data;
}

/**
   * (1) DONT FILL DIFFERENT MATRIX' COLUMNS/ROWS WITH 
   * THE SAME _VECTOR* - YOU WILL CAUSE AN ERROR !!!
   * TODO: Does it worth it to copy a passed vector to new memory
   * so that destroing original vector wouldn't destroy it in the matrix? 
   * I believe it's far more reasonable to comply with (1)
**/
void m_addRow(_matrix *m, int index, _vector* v) {
   int i;
   if(index < 0) return;
   if(index >= m->height) { //Fill missing vector-rows  with "NULL"
      m->rows = (_vector**)realloc(m->rows, sizeof(_vector*) * (index + 1));
      for(i = m->height; i < index; i++) {
         m->rows[i] = NULL;
      }
      m->height = index + 1;
   }
   else v_destroy(m->rows[index]); //If there was a previous vector
   m->rows[index] = v;
}

void m_addColumn(_matrix *m, int index, _vector* v) {
   int i;
   _vector* tmp;
   if(index < 0) return;
   if(v->length > m->height) { //Fill missing vector-rows  with "NULL"
      m->rows = (_vector**)realloc(m->rows, sizeof(_vector*)*v->length);
      for(i = m->height; i < v->length - 1; i++) {
         m->rows[i] = NULL;
      }
      m->height = v->length;
   }
   if(index >= m->width) {
      m->width = index + 1;
   }
   for(i = 0; i < v->length; i++) { //Adding by parsing "down" the matrix's last column
      if(!m->rows[i]) {
         v_init(&tmp, index + 1, v->cmpf, v->destrf);
         m->rows[i] = tmp;
      }
      v_put(m->rows[i], index, v->arr[i]);
   }
}

void v_sort(_vector *v) {
   qsort(v->arr, v->length, sizeof(void*), v->cmpf);
}

/**
    * Puts void* data into m[i][j]
    * Gets  Tcomparef cmpf  and Tdestroyf destrf to be put into 
    * i'th vector if it doesn't exist
**/
void m_put(_matrix* m, int i, int j, void* data) {
   int k;
   _vector* tmp;
   if(i < 0 || j < 0) return;
   if(i >= m->height) { //Fill missing vector-rows  with "NULL"
      m->rows = (_vector**)realloc(m->rows, sizeof(_vector*)*(i+1));
      for(k = m->height; k <= i; k++) {
         m->rows[k] = NULL;
      }
      m->height = i + 1;
   }
   if(j >= m->width) {
      m->width = j + 1;
   }   
   v_init(&tmp, j + 1, m->cmpf, m->destrf);
   m->rows[i] = tmp;  
   v_put(m->rows[i], j, data);
}

/**
    * Returns m[i][j]
**/
void* m_get(_matrix* m, int i, int j) {
   if(i < 0 || j < 0) return NULL;
   if(m->rows[i]) { //The row is non-NULL
      return  m->rows[i]->length > j ? m->rows[i]->arr[j] : NULL;
   }
   return NULL;
}
