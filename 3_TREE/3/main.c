#include "matrix.h"
#include "stdio.h"

#define Int(val) (*((int*)(val)))

void v_print(_vector *v){
    int i;
    printf("Vector(%d): \n", v->length);
    for(i=0;i<v->length;i++){
        if(v->arr[i]==NULL){ printf("NULL "); continue;}
        printf("%d ", Int(v->arr[i]));    
    }
    printf("\n");
}

void m_print(_matrix *m){
    int i,j;
    void* el;
    printf("Matrix(%dx%d): \n", m->width, m->height);
    for(i=0; i < m->height; i++){
        for(j=0; j<m->width; j++){
            if((el = m_get(m, i, j))) printf("  %d  ", Int(el));
            else printf("NULL ");        
        }
        printf("\n");
    }
    printf("\n");
}

//We're really getting void* *a and void* *b
//Will maybe be fixed by implementing TcmpfWrapper in future...
int cmpf(const void* a, const void* b){ //NULL is < then anything
    if(*((void**)a)==NULL){
        if(*((void**)b)==NULL) return 0;
             return -1;
    }
    if(*((void**)b)==NULL)return 1;
    return Int(*((void**)a)) - Int(*((void**)b));   
}

void destrf(const void* ptr){
    //Do nothing since int is not a pointer to any data    
}

int main(int argc, const char* argv[]){  
    int arr[5] = {3,4,5,1,2};  
    int i,j;
    
    _matrix* m;
    _vector* v;
    
    printf("-------------VECTOR:----------\n");
    v_init(&v, 1, cmpf, destrf);
    for(i=0;i<3;i++){ // Fill the vector
        v_put(v, i, &arr[i]);        
    }
    v_put(v,4,&arr[4]); 
    v_print(v);
    
    printf("-----Sort-------\n");
    v_sort(v);
    v_print(v);
    
    
    printf("-------------MATRIXES:----------\n");
    m_init(&m, v);
    m_print(m);  
    
    printf("-----Add Row-------\n");
    for(j=2;j<5;j++){ // Fill the matrix
         //DONT FILL DIFFERENT MATRIX' COLUMNS/ROWS WITH 
        //THE SAME _VECTOR* - YOU WILL CAUSE AN ERROR !!!        
        v_init(&v, 5, cmpf, destrf);
        for(i=0;i<3;i++){ // Fill the vector
            v_put(v, i, &arr[i]);        
        }           
        m_addRow(m, j, v);
    }
    m_print(m);
    
    printf("-----Add Column-------\n");  
    //DONT FILL DIFFERENT MATRIX' COLUMNS/ROWS WITH 
    //THE SAME _VECTOR* - YOU WILL CAUSE AN ERROR !!!    
   v_init(&v, 5, cmpf, destrf);
    for(i=1;i<4;i++){ // Fill the vector
        v_put(v, i, &arr[i]);        
    } 
    m_addColumn(m, m->width, v);
    m_print(m);   
     
    printf("-----Matrix Put(1,2), Matrix Put(5,5)------\n");
    m_put(m, 1, 2, &arr[0]);
    m_put(m, 5, 5, &arr[1]);     
    m_print(m);
    
    
    //We'll call the recursive m_destroy which will also free v 
    m_destroy(m); //Since v is already in matrix        
    return 0;
}
