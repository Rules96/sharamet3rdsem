//
#include <stdlib.h>
#include "AVL_tree.h"

AVL_node *new_node(int value) {
    AVL_node *n = calloc(1, sizeof *n);
    *n = (AVL_node) {value, 1, {AVL_NULL_node, AVL_NULL_node}};
    return n;
}

void set_height(AVL_node *n) {
    int h0 = n->kid[0] == NULL ? 0 : n->kid[0]->height;
    int h1 = n->kid[1] == NULL ? 0 : n->kid[1]->height;
    n->height = 1 + MAX(h0, h1);
}

int get_ballance(AVL_node *n) {
    int h0 = n->kid[0] == NULL ? 0 : n->kid[0]->height;
    int h1 = n->kid[1] == NULL ? 0 : n->kid[1]->height;
    return h0 - h1;
}

// rotate a subtree according to dir; if new root is nil, old root is freed
AVL_node *rotate(AVL_node **rootp, int dir) {
    AVL_node *old_r = *rootp, *new_r = old_r->kid[dir];

    if (AVL_NULL_node == (*rootp = new_r))
        free(old_r);
    else {
        old_r->kid[dir] = new_r->kid[!dir];
        set_height(old_r);
        new_r->kid[!dir] = old_r;
    }
    return new_r;
}

void adjust_balance(AVL_node **rootp) {
    AVL_node *root = *rootp;
    int b = get_ballance(root) / 2;
    if (b) {
        int dir = (1 - b) / 2;
        if (get_ballance(root->kid[dir]) == -b)
            rotate(&root->kid[dir], !dir);
        root = rotate(rootp, dir);
    }
    if (root != AVL_NULL_node) set_height(root);
}

// find the node that contains value as payload; or returns 0
AVL_node *query(AVL_node *root, int value) {
    return root == AVL_NULL_node
           ? 0
           : root->payload == value
             ? root
             : query(root->kid[value > root->payload], value);
}

void insert(AVL_node **rootp, int value) {
    AVL_node *root = *rootp;

    if (root == AVL_NULL_node)
        *rootp = new_node(value);
    else if (value != root->payload) { // don't allow dup keys
        insert(&root->kid[value > root->payload], value);
        adjust_balance(rootp);
    }
}

void delete(AVL_node **rootp, int value) {
    AVL_node *root = *rootp;
    if (root == AVL_NULL_node) return; // not found

    // if this is the node we want, rotate until off the tree
    if (root->payload == value)
        if (AVL_NULL_node == (root = rotate(rootp, get_ballance(root) < 0)))
            return;

    delete(&root->kid[value > root->payload], value);
    adjust_balance(rootp);
}