//aux display and verification routines, helpful but not essential
#include "AVL_tree_display.h"

void show_trunks(struct trunk *p) {
    if (!p) return;
    show_trunks(p->prev);
    printf("%s", p->str);
}

void show_tree(struct node *root, struct trunk *prev, int is_left) {
    if (root == AVL_NULL_node) return;

    struct trunk this_disp = {prev, "    "};
    char *prev_str = this_disp.str;
    show_tree(root->kid[0], &this_disp, 1);

    if (!prev)
        this_disp.str = "---";
    else if (is_left) {
        this_disp.str = ".--";
        prev_str = "   |";
    } else {
        this_disp.str = "`--";
        prev->str = prev_str;
    }

    show_trunks(&this_disp);
    printf("%d\n", root->payload);

    if (prev) prev->str = prev_str;
    this_disp.str = "   |";

    show_tree(root->kid[1], &this_disp, 0);
    if (!prev) puts("");
}

int verify(struct node *p) {
    if (p == AVL_NULL_node) return 1;

    int h0 = p->kid[0] == NULL ? 0 : p->kid[0]->height;
    int h1 = p->kid[1] == NULL ? 0 : p->kid[1]->height;
    int b = h0 - h1;

    if (p->height != 1 + MAX(h0, h1) || b < -1 || b > 1) {
        printf("node %d bad, balance %d\n", p->payload, b);
        show_tree(p, 0, 0);
        abort();
    }
    return verify(p->kid[0]) && verify(p->kid[1]);
}