//
#ifndef AVL_AVL_TREE_H
#define AVL_AVL_TREE_H

#define MAX(a, b) (((a) > (b)) ? (a) : (b))

typedef struct node {
    int payload;
    int height;
    struct node *kid[2];
} AVL_node;

// AVL_NULL_node should be used to mark NULL node
#define AVL_NULL_node NULL

AVL_node *new_node(int value);

void set_height(AVL_node *n);

int get_ballance(AVL_node *n);

AVL_node *rotate(AVL_node **rootp, int dir);

void adjust_balance(AVL_node **rootp);

AVL_node *query(AVL_node *root, int value);

void insert(AVL_node **rootp, int value);

void delete(AVL_node **rootp, int value);

#endif //AVL_AVL_TREE_H
