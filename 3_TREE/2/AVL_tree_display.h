//aux display and verification routines, helpful but not essential
#ifndef AVL_AVL_TREE_DISPLAY_H
#define AVL_AVL_TREE_DISPLAY_H

#include <stdio.h>
#include <stdlib.h>
#include "AVL_tree.h"

struct trunk {
    struct trunk *prev;
    char *str;
};

void show_trunks(struct trunk *p);

void show_tree(struct node *root, struct trunk *prev, int is_left);

int verify(struct node *p);

#endif //AVL_AVL_TREE_DISPLAY_H
